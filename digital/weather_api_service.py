from coordinates import Coordinates
from enum import Enum   # когда есть перечисление
from typing import NamedTuple
from datetime import datetime

Celsius = int


class WeatherType(Enum):
    THUNDERSTORM = 'Гроза'
    DRIZZLE = 'Изморозь'
    RAIN = 'Дождь'
    SNOW = 'Снег'
    CLEAR = 'Ясно'
    FOG = 'Туман'
    CLOUDS = 'Облачно'


# print(WeatherType.SNOW.name)
# print(WeatherType.SNOW.value)
# for weather_type in WeatherType:
#     print(weather_type.name, weather_type.value)

class Weather(NamedTuple):
    temperature: Celsius
    weather_type: WeatherType
    sunrise: datetime
    sunset: datetime
    city: str


def get_weather(coordinates: Coordinates) -> Weather:
    """
    Request weather in OpenWeather API and return it
    """
    return Weather(
        temperature=20,
        weather_type=WeatherType.RAIN,
        sunrise=datetime.fromisoformat('2022-06-04 04:00:00'),
        sunset=datetime.fromisoformat('2022-06-04 21:00:00'),
        city='Moscow'
    )
