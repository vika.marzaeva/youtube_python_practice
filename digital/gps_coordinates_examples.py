from typing import TypedDict, NamedTuple
from dataclasses import dataclass

from pympler import asizeof


class Coordinates(TypedDict):
    lat: float
    lon: float


class TupleCoordinates(NamedTuple):
    lat: float
    lon: float


@dataclass
class DataCoordinates:
    lon: float
    lat: float


print('Сколько занимает байт в оперативно памяти?')
coord_dict = Coordinates(**{'lat': 10, 'lon': 10})
coord_tuple = TupleCoordinates(lat=11, lon=11)
coord_data = DataCoordinates(lat=12, lon=12)
print("typed_dict", asizeof.asized(coord_dict).size)
print("named_tuple", asizeof.asized(coord_tuple).size)
print("dataclass", asizeof.asized(coord_data).size)


def get_gps_coordinates() -> Coordinates:
    """
    Return current coordinates using Ubuntu GPS
    """
    return Coordinates(**{'lat': 10, 'lon': 10})


def get_gps_tuple_coordinates() -> TupleCoordinates:
    """
    Return current coordinates using Ubuntu GPS
    """
    return TupleCoordinates(lat=11, lon=11)


def get_gps_data_coordinates() -> DataCoordinates:
    """
    Return current coordinates using Ubuntu GPS
    """
    return DataCoordinates(**{'lat': 12, 'lon': 12})


coordinates = get_gps_coordinates()
print(coordinates['lat'])
print(coordinates['lon'])

tuple_coordinates = get_gps_tuple_coordinates()
print(tuple_coordinates.lat)
print(tuple_coordinates.lon)

data_classes = get_gps_data_coordinates()
print(data_classes.lat)
print(data_classes.lon)

