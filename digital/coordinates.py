from typing import NamedTuple


class Coordinates(NamedTuple):
    lon: float
    lat: float


def get_coordinates() -> Coordinates:
    return Coordinates(lat=10, lon=10)
